//dileptrig
#include "DileptonTriggerTool/DileptonTriggerType.h"

//std/stl
#include <algorithm>
using namespace std;

namespace dileptrig
{

string DilepTrigType2Str(const DilepTrigType& type)
{
    string out = "DilepTrigInvalid";
    switch(type)
    {
        case DilepTrigType::WWBB1516 : out = "WWBB1516"; break;
        case DilepTrigType::WWBB151617 : out = "WWBB151617"; break;
        case DilepTrigType::WWBB15161718 : out = "WWBB15161718"; break;
        case DilepTrigType::WWBB151617RAND : out = "WWBB151617RAND"; break;
        case DilepTrigType::WWBB15161718RAND : out = "WWBB15161718RAND"; break;
        case DilepTrigType::DilepTrigInvalid : out = "DilepTrigInvalid"; break;
    } // switch
    return out;
}

DilepTrigType DilepTrigTypeFromStr(const std::string& type)
{
    DilepTrigType out = DilepTrigType::DilepTrigInvalid;
    if(type == "WWBB1516") out = DilepTrigType::WWBB1516;
    else if(type == "WWBB151617") out = DilepTrigType::WWBB151617;
    else if(type == "WWBB15161718") out = DilepTrigType::WWBB15161718;
    else if(type == "WWBB151617RAND") out = DilepTrigType::WWBB151617RAND;
    else if(type == "WWBB15161718RAND") out = DilepTrigType::WWBB15161718RAND;
    return out;
}

bool IsValidTrigType(std::string name)
{
    auto valid_types = ValidTrigTypes();
    return (std::find(valid_types.begin(), valid_types.end(), name) != valid_types.end());
}

vector<string> ValidTrigTypes()
{
    vector<string> out;
    for(size_t i = 0; i < DilepTrigType::DilepTrigInvalid; i++)
    {
        out.push_back(DilepTrigType2Str((DilepTrigType)i));
    } // i
    return out;
}

} // namespace dileptrig
