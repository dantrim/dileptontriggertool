//dileptrig
#include "DileptonTriggerTool/DileptonTriggerToolImp.h"

namespace dileptrig
{

DilepTrigFlavor DileptonTriggerToolImp::dilep_flavor( Susy::Lepton* l0,  Susy::Lepton* l1)
{
    if(l0 == nullptr || l1 == nullptr)
    {
        return DilepTrigFlavor::DilepTrigFlavorInvalid;
    }

    bool is_ee = (l0->isEle() && l1->isEle());
    bool is_mm = (l0->isMu() && l1->isMu());
    bool is_em = (l0->isEle() && l1->isMu());
    bool is_me = (l0->isMu() && l1->isEle());

    if(is_ee)
    {
        return DilepTrigFlavor::EE;
    }
    else if(is_mm)
    {
        return DilepTrigFlavor::MM;
    }
    else if(is_em)
    {
        return DilepTrigFlavor::EM;
    }
    else if(is_me)
    {
        return DilepTrigFlavor::ME;
    }
    else
    {
        return DilepTrigFlavor::DilepTrigFlavorInvalid;
    }
}

}
