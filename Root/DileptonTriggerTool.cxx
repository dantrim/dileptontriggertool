//dileptrig
#include "DileptonTriggerTool/DileptonTriggerTool.h"
#include "DileptonTriggerTool/trigger_tools/TriggerTools.h"
#include "DileptonTriggerTool/utility/helpers.h"

//std/stl
#include <iostream>
using namespace std;

namespace dileptrig
{
DileptonTriggerTool::DileptonTriggerTool()
{
}

bool DileptonTriggerTool::initialize(std::string trigger_type, TriggerTools* trigger_tool, SusyNtTools* nttools)
{
    cout << TRIGFUNC << "    Initializing DileptonTriggerTool with given type: "
        << trigger_type << endl;
    if(!IsValidTrigType(trigger_type))
    {
        cout << TRIGFUNC << "    ERROR Invalid DilepTrigType provided (=" << trigger_type << ")" << endl;
        cout << TRIGFUNC << "    ERROR  > valid DilepTrigType are: ";
        for(auto t : ValidTrigTypes()) cout << t << ", ";
        cout << endl;
        return false;
    }
    m_trigger_type = trigger_type;

    DilepTrigType type = DilepTrigTypeFromStr(trigger_type);
    if(type == DilepTrigType::WWBB1516 ||
        type == DilepTrigType::WWBB151617 ||
        type == DilepTrigType::WWBB15161718 ||
        type == DilepTrigType::WWBB151617RAND ||
        type == DilepTrigType::WWBB15161718RAND)
    {
        m_imp = std::make_unique<dileptrig::TriggerToolWWBB>();
    }
    else
    {
        cout << TRIGFUNC << "   ERROR There is no support for the requested DilepTrigType (=" << trigger_type << ")" << endl;
    }

    try
    {
        m_imp->initialize_triggertool(type, trigger_tool, nttools);
    }
    catch(std::exception& e)
    {
        cout << TRIGFUNC << "   ERROR TriggerTool failed to initialize: " << e.what() << endl;
        return false;
    }
    return true;

}

bool DileptonTriggerTool::pass_trigger_selection(Susy::Event* event,
            Susy::Lepton* lead_lepton, Susy::Lepton* sub_lead_lepton,
            float& sf, bool require_matching)
{
    return m_imp->pass_trigger_selection(event, {lead_lepton, sub_lead_lepton}, sf, require_matching);
}

std::string DileptonTriggerTool::trigger_type()
{
    return m_trigger_type;
}

} //  namespace dileptrig
