//dileptrig
#include "DileptonTriggerTool/trigger_tools/TriggerToolWWBB.h"
#include "DileptonTriggerTool/utility/helpers.h"
#include "SusyNtuple/SusyNtTools.h"


namespace dileptrig
{

TriggerToolWWBB::TriggerToolWWBB()
{
    m_type = DilepTrigType::DilepTrigInvalid;
}

void TriggerToolWWBB::initialize_triggertool(DilepTrigType type, TriggerTools* trigger_tool, SusyNtTools* nttools)
{
    m_type = type;
    m_trigger_tool = trigger_tool;
    m_nttools = nttools;
    uniform_distribution = std::uniform_real_distribution<float>(0.0, 1.0);

    cout << TRIGFUNC << "   Initializing TriggerToolWWBB (type = " << DilepTrigType2Str(type) << ")" << endl;
}

bool TriggerToolWWBB::pass_trigger_selection(Susy::Event* event,
            std::vector<Susy::Lepton*> leptons, float& sf, bool require_matching)
{
    if(type() == DilepTrigType::WWBB1516)
    {
        return pass_logic_1516(event, leptons, sf, require_matching);
    }
    else if(type() == DilepTrigType::WWBB151617)
    {
        return pass_logic_151617(event, leptons, sf, require_matching);
    }
    else if(type() == DilepTrigType::WWBB15161718)
    {
        return pass_logic_15161718(event, leptons, sf, require_matching);
    }
    else if(type() == DilepTrigType::WWBB151617RAND)
    {
        return pass_logic_151617(event, leptons, sf, require_matching, true);
    }
    else if(type() == DilepTrigType::WWBB15161718RAND)
    {
        return pass_logic_15161718(event, leptons, sf, require_matching, true);
    }
    else
    {
        cout << TRIGFUNC << "   ERROR Unexpected DilepTrigType: " << DilepTrigType2Str(type()) << endl;
        exit(1);
    }

    return false;
}

bool TriggerToolWWBB::pass_logic_1516(Susy::Event* evt, vector<Susy::Lepton*> leptons,
        float& sf, bool require_matching)
{
    bool trig_fired = false;

    int year = evt->treatAsYear;
    DilepTrigFlavor flav = dilep_flavor(leptons.at(0), leptons.at(1));

    if(year == 2015)
    {
        trig_fired = pass_triggers_15(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }
    else if(year == 2016)
    {
        trig_fired = pass_triggers_16(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }
    

    return trig_fired;
}
bool TriggerToolWWBB::pass_logic_151617(Susy::Event* evt, vector<Susy::Lepton*> leptons,
        float& sf, bool require_matching, bool use_random)
{
    bool trig_fired = false;

    int year = evt->treatAsYear;
    DilepTrigFlavor flav = dilep_flavor(leptons.at(0), leptons.at(1));

    if(year == 2015)
    {
        trig_fired = pass_triggers_15(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }
    else if(year == 2016)
    {
        trig_fired = pass_triggers_16(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }
    else if(year == 2017)
    {
        trig_fired = pass_triggers_17(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching, use_random);
    }

    return trig_fired;
}
bool TriggerToolWWBB::pass_logic_15161718(Susy::Event* evt, vector<Susy::Lepton*> leptons,
        float& sf, bool require_matching, bool use_random)
{
    bool trig_fired = false;

    int year = evt->treatAsYear;
    DilepTrigFlavor flav = dilep_flavor(leptons.at(0), leptons.at(1));

    if(year == 2015)
    {
        trig_fired = pass_triggers_15(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }
    else if(year == 2016)
    {
        trig_fired = pass_triggers_16(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }
    else if(year == 2017)
    {
        trig_fired = pass_triggers_17(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching, use_random);
    }
    else if(year == 2018)
    {
        trig_fired = pass_triggers_18(evt, flav, leptons.at(0), leptons.at(1), sf, require_matching);
    }

    return trig_fired;
}

DileptonTrigSFIdx TriggerToolWWBB::get_dilep_sf_idx(Susy::Lepton* lead, Susy::Lepton* sub)
{
    DileptonTrigSFIdx trig_idx = 0;
    trig_idx |= (uint8_t)(lead->idx);
    trig_idx = (trig_idx << 8);
    trig_idx |= (uint8_t)(sub->idx);
    return trig_idx;
}

bool TriggerToolWWBB::pass_triggers_15(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub,
        float& sf, bool require_matching)
{

    float pt0 = lead->Pt();
    float pt1 = sub->Pt();
    auto trig_idx = get_dilep_sf_idx(lead, sub);

    if(flav == DilepTrigFlavor::EE)
    {
        bool pe0 = (pt0 >= 26 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e24_lhmedium_L1EM20VH"));
        bool pe1 = (pt1 >= 26 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e24_lhmedium_L1EM20VH"));
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e24_lhmedium_L1EM20VH");
            return true;
        }
        else if(pe1)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e24_lhmedium_L1EM20VH");
            return true;
        }
        else if( (pt0 >= 14 && pt1 >= 14) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e12_lhloose_L12EM10VH") )
        {
            sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::MM)
    {
        bool pm0 = (pt0 >= 22 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu20_iloose_L1MU15"));
        bool pm1 = (pt1 >= 22 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu20_iloose_L1MU15"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        auto mu1 = dynamic_cast<Susy::Muon*>(sub);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu20_iloose_L1MU15");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu20_iloose_L1MU15");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu18_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_mm.count(trig_idx) ? evt->m_dilepton_trigger_sf_mm.at(trig_idx) : 1.0); 
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::EM)
    {
        bool pe0 = (pt0 >= 26 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e24_lhmedium_L1EM20VH"));
        bool pm1 = (pt1 >= 22 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu20_iloose_L1MU15"));
        auto mu0 = dynamic_cast<Susy::Muon*>(sub);
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e24_lhmedium_L1EM20VH");
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu20_iloose_L1MU15");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        //else if( (pt0 >= 26 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e24_lhmedium_L1EM20VHI_mu8noL1"))
        //{
        //    return true;
        //}
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::ME)
    {
        bool pm0 = (pt0>=22 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu20_iloose_L1MU15"));
        bool pe0 = (pt1>=26 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e24_lhmedium_L1EM20VH"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu20_iloose_L1MU15");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e24_lhmedium_L1EM20VH");
            return true;
        }
        else if( (pt0 >= 26 && pt1 >= 9) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e7_lhmedium_mu24"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else
    {
        cout << TRIGFUNC << "   ERROR: Invalid condition met" << endl;
        exit(1);
    }

}
bool TriggerToolWWBB::pass_triggers_16(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub,
        float& sf, bool require_matching)
{
    float pt0 = lead->Pt();
    float pt1 = sub->Pt();
    auto trig_idx = get_dilep_sf_idx(lead, sub);
    if(flav == DilepTrigFlavor::EE)
    {
        bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        bool pe1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if(pe1)
        {
            sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if( (pt0 >= 19 && pt1 >= 19) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e17_lhvloose_nod0"))
        {
            sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::MM)
    {
        bool pm0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        bool pm1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        auto mu1 = dynamic_cast<Susy::Muon*>(sub);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 24 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu22_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_mm.count(trig_idx) ? evt->m_dilepton_trigger_sf_mm.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::EM)
    {
        bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        bool pm1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        auto mu0 = dynamic_cast<Susy::Muon*>(sub);
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 28 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_nod0_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::ME)
    {
        bool pm0 = (pt0>=28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        bool pe0 = (pt1>=28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if( (pt0 >= 26 && pt1 >= 9) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e7_lhmedium_nod0_mu24"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_nod0_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else
    {
        cout << TRIGFUNC << "   ERROR: Invalid condition met" << endl;
        exit(1);
    }

}
bool TriggerToolWWBB::pass_triggers_17(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub,
        float& sf, bool require_matching, bool use_random)
{
    float pt0 = lead->Pt();
    float pt1 = sub->Pt();
    bool isData = !evt->isMC;
    int run_number = evt->run;
    auto trig_idx = get_dilep_sf_idx(lead, sub);

    if(flav == DilepTrigFlavor::EE)
    {
        if(!use_random)
        {
            bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
            bool pe1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
            if(pe0)
            {
                sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
                return true;
            }
            else if(pe1)
            {
                sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
                return true;
            }
            else if( (pt0 >= 26 && pt1 >= 26) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e24_lhvloose_nod0"))
            {
                sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
                return true;
            }
            else
            {
                sf = 1.0;
                return false;
            }
        }
        else
        {
            bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
            bool pe1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
            if(pe0)
            {
                sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
                return true;
            }
            else if(pe1)
            {
                sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
                return true;
            }
            else if(!isData)
            {
                float random_number = uniform_distribution(rng);
                if(random_number < (0.6 / 140.))
                {
                    if( (pt0 >= 26 && pt1 >= 26) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e24_lhvloose_nod0"))
                    {
                        sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
                        return true;
                    }
                }
                else if( (pt0 >= 19 && pt1 >= 19) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e17_lhvloose_nod0_L12EM15VHI"))
                {
                    
                    sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
                    return true;
                }
                else
                {
                    sf = 1.0;
                    return false;
                }
            }
            else
            {
                if(run_number >= 326834 && run_number <= 328393)
                {
                    if( (pt0 >= 26 && pt1 >= 26) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e24_lhvloose_nod0"))
                    {
                        sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
                        return true;
                    }
                }
                else if( (pt0 >= 19 && pt1 >= 19) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e17_lhvloose_nod0_L12EM15VHI"))
                {
                    sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    else if(flav == DilepTrigFlavor::MM)
    {
        bool pm0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        bool pm1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        auto mu1 = dynamic_cast<Susy::Muon*>(sub);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 24 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu22_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_mm.count(trig_idx) ? evt->m_dilepton_trigger_sf_mm.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::EM)
    {
        bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        bool pm1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        auto mu0 = dynamic_cast<Susy::Muon*>(sub);
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 28 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhmedium_nod0_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_nod0_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::ME)
    {
        bool pm0 = (pt0>=28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        bool pe0 = (pt1>=28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if( (pt0 >= 26 && pt1 >= 9) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e7_lhmedium_nod0_mu24"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_nod0_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else
    {
        cout << TRIGFUNC << "   ERROR: Invalid condition met" << endl;
        exit(1);
    }

    return false;

}
bool TriggerToolWWBB::pass_triggers_18(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub,
        float& sf, bool require_matching)
{
    float pt0 = lead->Pt();
    float pt1 = sub->Pt();
    auto trig_idx = get_dilep_sf_idx(lead, sub);
    if(flav == DilepTrigFlavor::EE)
    {
        bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        bool pe1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if(pe1)
        {
            sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if( (pt0 >= 19 && pt1 >= 19) && trigger_tool()->passTrigger(evt->trigBits, "HLT_2e17_lhvloose_nod0_L12EM15VHI"))
        {
            sf = (evt->m_dilepton_trigger_sf_ee.count(trig_idx) ? evt->m_dilepton_trigger_sf_ee.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::MM)
    {
        bool pm0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        bool pm1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        auto mu1 = dynamic_cast<Susy::Muon*>(sub);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium"); 
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu26_ivarmedium"); 
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 24 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu22_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_mm.count(trig_idx) ? evt->m_dilepton_trigger_sf_mm.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::EM)
    {
        bool pe0 = (pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        bool pm1 = (pt1 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        auto mu0 = dynamic_cast<Susy::Muon*>(sub);
        if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(lead, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if(pm1)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(sub, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if( (pt0 >= 28 && pt1 >= 10) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhmedium_nod0_mu8noL1"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_nod0_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else if(flav == DilepTrigFlavor::ME)
    {
        //if( pt0 >= 28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"))

        bool pm0 = (pt0>=28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_mu26_ivarmedium"));
        bool pe0 = (pt1>=28 && trigger_tool()->passTrigger(evt->trigBits, "HLT_e26_lhtight_nod0_ivarloose"));
        auto mu0 = dynamic_cast<Susy::Muon*>(lead);
        if(pm0)
        {
            //sf = nttools()->get_muon_trigger_scale_factor(lead, "HLT_mu26_ivarmedium");
            sf = mu0->muoTrigSF_map["HLT_mu26_ivarmedium"];
            return true;
        }
        else if(pe0)
        {
            sf = nttools()->get_electron_trigger_scale_factor(sub, "HLT_e26_lhtight_nod0_ivarloose");
            return true;
        }
        else if( (pt0 >= 26 && pt1 >= 9) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e7_lhmedium_nod0_mu24"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else if( (pt0 >= 20 && pt1 >= 17) && trigger_tool()->passTrigger(evt->trigBits, "HLT_e17_lhloose_nod0_mu14"))
        {
            sf = (evt->m_dilepton_trigger_sf_df.count(trig_idx) ? evt->m_dilepton_trigger_sf_df.at(trig_idx) : 1.0);
            return true;
        }
        else
        {
            sf = 1.0;
            return false;
        }
    }
    else
    {
        cout << TRIGFUNC << "   ERROR: Invalid condition met" << endl;
        exit(1);
    }

}

} // namespace dileptrig

