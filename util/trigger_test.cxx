//DileptonTriggerTool
#include "DileptonTriggerTool/DileptonTriggerTool.h"

//SusyNtuple
#include "SusyNtuple/ChainHelper.h"
#include "SusyNtuple/SusyNtTools.h"

//std/stl
#include<iostream>
using namespace std;


int main()
{
    string test_file = "/data/uclhc/uci/user/dantrim/susynt_productions/n0306/mc/group.phys-susy.mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.SusyNt.mc16a.p3652_n0306_nt/group.phys-susy.16183127._000021.susyNt.root";
    test_file = "/data/uclhc/uci/user/dantrim/susynt_productions/n0306/data/group.phys-susy.data17_13TeV.00333828.physics_Main.SusyNt.p3637_n0306_nt/group.phys-susy.16181904._000001.susyNt.root";
    cout << "opening: " << test_file << endl;

    SusyNtTools tools;
    tools.initTriggerTool(test_file);

    dileptrig::DileptonTriggerTool* trigger_tool = new dileptrig::DileptonTriggerTool();
    if(!trigger_tool->initialize("WWBB15161718RAND", &tools.triggerTool(), &tools))
    {
        cout << "FAILED" << endl;
    }
    cout << "SUCCESS" << endl;

    delete trigger_tool;
    return 0;
}
