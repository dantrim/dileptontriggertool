#ifndef DILEPTRIG_HELPERS
#define DILEPTRIG_HELPERS

#include <string>

std::string computeMethodName(const std::string& function, const std::string& prettyFunction);
#define TRIGFUNC computeMethodName(__FUNCTION__,__PRETTY_FUNCTION__).c_str()

#endif
