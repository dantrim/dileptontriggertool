#ifndef DILEP_TRIG_TOOL_H
#define DILEP_TRIG_TOOL_H

//DileptonTriggerTool
#include "DileptonTriggerTool/DileptonTriggerToolImp.h"

//std/stl
#include <string>
#include <memory>

//ROOT
#include "TLorentzVector.h"

//SusyNtuple
#include "SusyNtuple/Event.h"
#include "SusyNtuple/Lepton.h"
#include "SusyNtuple/TriggerTools.h"

namespace dileptrig
{

class DileptonTriggerTool
{
    public :
        DileptonTriggerTool();
        bool initialize(std::string trigger_type, TriggerTools* trigger_tool, SusyNtTools* tools);
        std::string trigger_type();

        // main analysis methods
        bool pass_trigger_selection(Susy::Event* evt,
                    Susy::Lepton* lead_lepton,
                    Susy::Lepton* sub_lead_lepton,
                    float& scale_factor,
                    bool require_matching = false);

    private :
        std::string m_trigger_type;
        std::unique_ptr<dileptrig::DileptonTriggerToolImp> m_imp;

}; // class DileptonTriggerTool

} // namespace dileptrig

#endif
