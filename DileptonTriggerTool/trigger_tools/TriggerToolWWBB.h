#ifndef TRIGGERTOOL_WWBB_H
#define TRIGGERTOOL_WWBB_H

//dileptrig
#include "DileptonTriggerTool/DileptonTriggerToolImp.h"
#include "DileptonTriggerTool/DileptonTriggerType.h"

//SusyNtuple
#include "SusyNtuple/TriggerTools.h"

//std/stl
#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <random>
using namespace std;

namespace dileptrig
{

class TriggerToolWWBB : public DileptonTriggerToolImp
{
    public :
        explicit TriggerToolWWBB();
        void initialize_triggertool(DilepTrigType type, TriggerTools* trigger_tool, SusyNtTools* nttools);
        bool pass_trigger_selection(Susy::Event* event,
                std::vector<Susy::Lepton*> leptons, float& scale_factor, bool require_matching);

    private :

        DileptonTrigSFIdx get_dilep_sf_idx(Susy::Lepton* lead, Susy::Lepton* sub);

        bool pass_logic_1516(
                    Susy::Event* evt, std::vector<Susy::Lepton*> leptons,
                    float& scale_factor, bool require_matching);
        bool pass_logic_151617(
                    Susy::Event* evt, std::vector<Susy::Lepton*> leptons,
                    float& scale_factor, bool require_matching, bool random = false);
        bool pass_logic_15161718(
                    Susy::Event* evt, std::vector<Susy::Lepton*> leptons,
                    float& scale_factor, bool require_matching, bool random = false);

        // individual by year
        bool pass_triggers_15(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub, float& scale_factor, bool require_matching);
        bool pass_triggers_16(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub, float& scale_factor, bool require_matching);
        bool pass_triggers_17(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub, float& scale_factor, bool require_matching, bool use_random);
        bool pass_triggers_18(Susy::Event* evt, DilepTrigFlavor flav, Susy::Lepton* lead, Susy::Lepton* sub, float& scale_factor, bool require_matching);

        std::default_random_engine rng;
        std::uniform_real_distribution<float> uniform_distribution; //(0.0, 1.0);


}; // class TriggerToolWWBB

} // namespace dileptrig


#endif
