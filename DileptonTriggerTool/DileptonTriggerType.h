#ifndef DILEP_TRIG_TYPE_H
#define DILEP_TRIG_TYPE_H

#include <vector>
#include <string>

namespace dileptrig
{
    enum DilepTrigType
    {
        WWBB1516=0
        ,WWBB151617
        ,WWBB151617RAND
        ,WWBB15161718
        ,WWBB15161718RAND
        ,DilepTrigInvalid
    }; // enum DilepTrigType
    std::string DilepTrigType2Str(const DilepTrigType& type);
    DilepTrigType DilepTrigTypeFromStr(const std::string& t);
    bool IsValidTrigType(std::string name);
    std::vector<std::string> ValidTrigTypes();
} // namespace dileptrig

#endif
