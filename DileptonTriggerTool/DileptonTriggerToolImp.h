#ifndef DILEPTRIG_IMP_H
#define DILEPTRIG_IMP_H

//dileptrig
#include "DileptonTriggerTool/DileptonTriggerType.h"

//std/stl
#include <string>
#include <vector>

//SusyNtuple
#include "SusyNtuple/Event.h"
#include "SusyNtuple/Lepton.h"
#include "SusyNtuple/TriggerTools.h"
#include "SusyNtuple/SusyNtTools.h"

namespace dileptrig
{
        enum DilepTrigFlavor
        {
            EE=0
            ,MM
            ,EM
            ,ME
            ,DilepTrigFlavorInvalid
        };


class DileptonTriggerToolImp
{
    public :
        virtual void initialize_triggertool(DilepTrigType type, TriggerTools* trigger_tool, SusyNtTools* nttools) = 0;
        virtual bool pass_trigger_selection(Susy::Event* event,
            std::vector<Susy::Lepton*> leptons, float& scale_factor,  bool require_matching) = 0;

    protected :

        DilepTrigFlavor dilep_flavor(Susy::Lepton* l0, Susy::Lepton* l1);
        TriggerTools* m_trigger_tool;
        TriggerTools* trigger_tool() { return m_trigger_tool; }
        SusyNtTools* m_nttools;
        SusyNtTools* nttools() { return m_nttools; }
        DilepTrigType m_type;
        DilepTrigType type() { return m_type; }

};  // class DileptonTriggerToolImp

} // namespcae dileptrig

#endif
